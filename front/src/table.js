import THead from './thead';
import TBody from './tbody';
import Pager from './pager';

export default class Table{

  constructor(containerId, options = {}){
    this.options = options;
    this.container = document.getElementById(containerId);
    if (!this.container) {
      throw `Container with ID '${containerId}' not found`;
      return;
    }
    this.columns = options.columns || {};
    if (Object.keys(this.columns).length < 1) {
      throw `You must set columns`;
      return;
    }

    this.endpoint = options.endpoint;
    if (!this.endpoint) {
      throw `You must set endpoint url`;
      return;
    }
    this._table = document.createElement("table");
    this._table.classList.add("table");
    this.thead = new THead(this);
    this.tbody = new TBody(this);
    this.pager = new Pager(this);

    this.data = [];

    this.render();
  }

  render(){
    if (this.options.title) {
      this.header = document.createElement("h2");
      this.header.innerText = this.options.title;
      this.container.appendChild(this.header);
    }
    this.container.appendChild(this._table);
    this.pager.render();

    this.paginate();
  }

  async loadData(page){
    let response = await fetch(`${this.endpoint}?page=${page}`);
    return await response.json();
  }

  async paginate(page = 1){
    this.data = this.data.concat(await this.loadData(page));
    this.search();
  }

  search(){
    let data = this.data;
    this.thead.ths.forEach((th) => {
      if (th.term.length > 0) {
        data = data.filter((row) => row[th.name].toString().toUpperCase().includes(th.term.toUpperCase()))
      }
    })
    this.tbody.iterate(this.sort([ ...data ]));
  }

  sort(data){
    if (this.sortKey) {
      return data.sort((r1, r2) => r1[this.sortKey] > r2[this.sortKey] ? this.sortDirection : this.sortDirection * -1);
    } else {
      return data;
    }
  }

  setOrder(key, direction){
    if (this.sortKey === key && this.sortDirection === direction) {
      this.sortKey = null;
      this.sortDirection = null;
    } else {
      this.sortKey = key;
      this.sortDirection = direction;
    }
    this.search();
  }

}

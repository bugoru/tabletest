export default class Th{

  constructor(thead, name, title){
    this.thead = thead;
    this.name = name;
    this.title = title;
    this.term = "";
    this._th = document.createElement("th");
    this._text = document.createElement("p");
    this._input = document.createElement("input");
    this._asc = document.createElement("span");
    this._desc = document.createElement("span");
    this.search = this.search.bind(this);
    this.addListeners();
    this.render();
  }

  addListeners(){
    [ 'change', 'keyup', 'mouseup' ].forEach((e) => this._input.addEventListener(e, this.search));
    [ 'paste', 'cut' ].forEach((e) => this._input.addEventListener(e, () => setTimeout(this.search, 100)));
  }

  render(){
    this.thead._tr.appendChild(this._th);
    this._text.innerHTML = this.title;
    this._asc.className = "asc";
    this._desc.className = "desc";
    this._asc.onclick = () => this.sort(1);
    this._desc.onclick = () => this.sort(-1);
    this._th.appendChild(this._asc);
    this._th.appendChild(this._text);
    this._th.appendChild(this._desc);
    this._th.appendChild(this._input);
  }

  search(){
    if (this.term !== this._input.value){
      this.term = this._input.value;
      this.thead.table.search();
    }
  }

  sort(direction){
    this.thead.table.setOrder(this.name, direction);
  }

}

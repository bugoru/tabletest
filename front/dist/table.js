var JSTest = (function (exports) {
  'use strict';

  class Th{

    constructor(thead, name, title){
      this.thead = thead;
      this.name = name;
      this.title = title;
      this.term = "";
      this._th = document.createElement("th");
      this._text = document.createElement("p");
      this._input = document.createElement("input");
      this._asc = document.createElement("span");
      this._desc = document.createElement("span");
      this.search = this.search.bind(this);
      this.addListeners();
      this.render();
    }

    addListeners(){
      [ 'change', 'keyup', 'mouseup' ].forEach((e) => this._input.addEventListener(e, this.search));
      [ 'paste', 'cut' ].forEach((e) => this._input.addEventListener(e, () => setTimeout(this.search, 100)));
    }

    render(){
      this.thead._tr.appendChild(this._th);
      this._text.innerHTML = this.title;
      this._asc.className = "asc";
      this._desc.className = "desc";
      this._asc.onclick = () => this.sort(1);
      this._desc.onclick = () => this.sort(-1);
      this._th.appendChild(this._asc);
      this._th.appendChild(this._text);
      this._th.appendChild(this._desc);
      this._th.appendChild(this._input);
    }

    search(){
      if (this.term !== this._input.value){
        this.term = this._input.value;
        this.thead.table.search();
      }
    }

    sort(direction){
      this.thead.table.setOrder(this.name, direction);
    }

  }

  class THead {

    constructor(table){
      this.table = table;
      this.columns = table.columns;
      this._thead = document.createElement("thead");
      this._tr = document.createElement("tr");
      this.ths = Object.entries(this.columns).map(([ name, title ]) => new Th(this, name, title));
      this.render();
    }

    render(){
      this.table._table.appendChild(this._thead);
      this._thead.appendChild(this._tr);
    }

  }

  class TBody{

    constructor(table){
      this.table = table;
      this.columns = table.columns;
      this._tbody = document.createElement("tbody");
      this.render();
    }

    render(){
      this.table._table.appendChild(this._tbody);
    }

    iterate(data = []){
      this._tbody.innerHTML = "";
      data.forEach((dataRow) => {
        let row = document.createElement("tr");
        this._tbody.appendChild(row);
        Object.keys(this.columns).forEach((key) => {
          let td = document.createElement("td");
          let value = dataRow[key] == undefined ? "" : dataRow[key];
          td.innerHTML = value;
          row.appendChild(td);
        });
      });
    }

  }

  class Pager{

    constructor(table){
      this.table = table;
      this.page = 1;
      this.button = document.createElement("div");
      this.button.onclick = () => this.nextPage();
    }

    render(){
      this.button.className = "pager";
      this.button.innerHTML = "Показать еще";
      this.table.container.appendChild(this.button);
    }

    nextPage(){
      this.page = this.page + 1;
      this.table.paginate(this.page);
    }

  }

  class Table{

    constructor(containerId, options = {}){
      this.options = options;
      this.container = document.getElementById(containerId);
      if (!this.container) {
        throw `Container with ID '${containerId}' not found`;
        return;
      }
      this.columns = options.columns || {};
      if (Object.keys(this.columns).length < 1) {
        throw `You must set columns`;
        return;
      }

      this.endpoint = options.endpoint;
      if (!this.endpoint) {
        throw `You must set endpoint url`;
        return;
      }
      this._table = document.createElement("table");
      this._table.classList.add("table");
      this.thead = new THead(this);
      this.tbody = new TBody(this);
      this.pager = new Pager(this);

      this.data = [];

      this.render();
    }

    render(){
      if (this.options.title) {
        this.header = document.createElement("h2");
        this.header.innerText = this.options.title;
        this.container.appendChild(this.header);
      }
      this.container.appendChild(this._table);
      this.pager.render();

      this.paginate();
    }

    async loadData(page){
      let response = await fetch(`${this.endpoint}?page=${page}`);
      return await response.json();
    }

    async paginate(page = 1){
      this.data = this.data.concat(await this.loadData(page));
      this.search();
    }

    search(){
      let data = this.data;
      this.thead.ths.forEach((th) => {
        if (th.term.length > 0) {
          data = data.filter((row) => row[th.name].toString().toUpperCase().includes(th.term.toUpperCase()));
        }
      });
      this.tbody.iterate(this.sort([ ...data ]));
    }

    sort(data){
      if (this.sortKey) {
        return data.sort((r1, r2) => r1[this.sortKey] > r2[this.sortKey] ? this.sortDirection : this.sortDirection * -1);
      } else {
        return data;
      }
    }

    setOrder(key, direction){
      if (this.sortKey === key && this.sortDirection === direction) {
        this.sortKey = null;
        this.sortDirection = null;
      } else {
        this.sortKey = key;
        this.sortDirection = direction;
      }
      this.search();
    }

  }

  exports.Table = Table;

  return exports;

}({}));

import Th from './th';

export default class THead {

  constructor(table){
    this.table = table;
    this.columns = table.columns;
    this._thead = document.createElement("thead");
    this._tr = document.createElement("tr");
    this.ths = Object.entries(this.columns).map(([ name, title ]) => new Th(this, name, title))
    this.render();
  }

  render(){
    this.table._table.appendChild(this._thead);
    this._thead.appendChild(this._tr);
  }

}

package api;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class Row {

    private final int id;

    public Row(int id) {
      this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
      Integer a = new Integer(10);
      return a.toString(2730 + id, 16).toUpperCase();
    }

    public Double getValue(){
      BigDecimal value = new BigDecimal(Math.random() + 1);
      value = value.setScale(2, RoundingMode.HALF_UP);
      return value.doubleValue();
    }

}

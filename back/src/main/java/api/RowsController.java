package api;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
public class RowsController {

  @CrossOrigin(origins = "*")
  @GetMapping("/rows")
  public Row[] row(@RequestParam(value="page", defaultValue="1") int page) {
    Row[] rows = new Row[10];
    for (int i=0; i < rows.length; i++){
      rows[i] = new Row((page-1)*10 +i);
    }
    return rows;
  }

}

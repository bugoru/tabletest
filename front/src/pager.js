export default class Pager{

  constructor(table){
    this.table = table;
    this.page = 1;
    this.button = document.createElement("div");
    this.button.onclick = () => this.nextPage();
  }

  render(){
    this.button.className = "pager";
    this.button.innerHTML = "Показать еще";
    this.table.container.appendChild(this.button);
  }

  nextPage(){
    this.page = this.page + 1;
    this.table.paginate(this.page);
  }

}

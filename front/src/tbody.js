export default class TBody{

  constructor(table){
    this.table = table;
    this.columns = table.columns;
    this._tbody = document.createElement("tbody");
    this.render();
  }

  render(){
    this.table._table.appendChild(this._tbody);
  }

  iterate(data = []){
    this._tbody.innerHTML = "";
    data.forEach((dataRow) => {
      let row = document.createElement("tr");
      this._tbody.appendChild(row);
      Object.keys(this.columns).forEach((key) => {
        let td = document.createElement("td");
        let value = dataRow[key] == undefined ? "" : dataRow[key];
        td.innerHTML = value;
        row.appendChild(td);
      })
    })
  }

}
